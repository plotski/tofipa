import collections
import configparser
import itertools
import os
import re
from unittest.mock import Mock, call

import aiobtclientapi
import pytest

from tofipa import _config, _errors


def test_as_dict(mocker):
    mock_cfg = Mock(
        sections=Mock(return_value=('foo', 'bar', 'baz')),
        items=Mock(side_effect=(
            ((f'key:{x}.{y}', f'value:{x}.{y}') for y in range(3))
            for x in range(10, 40, 10)
        )),
    )
    return_value = _config._as_dict(mock_cfg)
    assert return_value == {
        'foo': {
            'key:10.0': 'value:10.0',
            'key:10.1': 'value:10.1',
            'key:10.2': 'value:10.2',
        },
        'bar': {
            'key:20.0': 'value:20.0',
            'key:20.1': 'value:20.1',
            'key:20.2': 'value:20.2',
        },
        'baz': {
            'key:30.0': 'value:30.0',
            'key:30.1': 'value:30.1',
            'key:30.2': 'value:30.2',
        },
    }


@pytest.mark.parametrize(
    argnames='options, option, exp_exception',
    argvalues=(
        ({'client': 'deluge'}, 'client', None),
        ({'client': 'foo'}, 'client', ValueError('foo: Unknown client')),
    ),
    ids=lambda v: repr(v),
)
def test_convert_to_client(options, option, exp_exception, mocker):
    mocker.patch('aiobtclientapi.client_names', return_value=('deluge', 'transmission'))

    if exp_exception:
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            _config._convert_to_client(options, option)
    else:
        return_value = _config._convert_to_client(options, option)
        assert return_value is options[option]


@pytest.mark.parametrize(
    argnames='options, option, exp_type, exp_exception',
    argvalues=(
        ({'client': 'deluge', 'url': 'localhost:123'}, 'url', aiobtclientapi.DelugeAPI.URL, None),
        ({'client': 'deluge', 'url': 'file://path'}, 'url', None, ValueError("file://path: Deluge URLs don't have a scheme")),
    ),
    ids=lambda v: repr(v),
)
def test_convert_to_url(options, option, exp_type, exp_exception, mocker):
    mocker.patch('aiobtclientapi.client_names', return_value=('deluge', 'transmission'))

    if exp_exception:
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            _config._convert_to_url(options, option)
    else:
        return_value = _config._convert_to_url(options, option)
        assert isinstance(return_value, exp_type)
        assert str(return_value) == str(options[option])


@pytest.mark.parametrize(
    argnames='options, option, exp_result',
    argvalues=(
        ({'foo': 'arf'}, 'foo', ValueError('arf: Must be true/false, yes/no, on/off, 1/0')),
        ({'foo': 'true'}, 'foo', True),
        ({'foo': 'false'}, 'foo', False),
        ({'foo': 'yes'}, 'foo', True),
        ({'foo': 'no'}, 'foo', False),
        ({'foo': 'on'}, 'foo', True),
        ({'foo': 'off'}, 'foo', False),
        ({'foo': '1'}, 'foo', True),
        ({'foo': '0'}, 'foo', False),
    ),
    ids=lambda v: repr(v),
)
def test_convert_to_bool(options, option, exp_result, mocker):
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            _config._convert_to_bool(options, option)
    else:
        return_value = _config._convert_to_bool(options, option)
        assert return_value is exp_result


@pytest.mark.parametrize(
    argnames='options, option, exp_result',
    argvalues=(
        ({'foo': 1}, 'foo', '1'),
        ({'foo': [1, '2', [3]]}, 'foo', "[1, '2', [3]]"),
        ({'foo': 'hey\nyou'}, 'foo', 'hey you'),
    ),
    ids=lambda v: repr(v),
)
def test_convert_to_string(options, option, exp_result, mocker):
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            _config._convert_to_string(options, option)
    else:
        return_value = _config._convert_to_string(options, option)
        assert return_value == exp_result


@pytest.mark.parametrize(
    argnames='options, option, exp_result',
    argvalues=(
        ({'foo': ''}, 'foo', []),
        ({'foo': '1'}, 'foo', ['1']),
        ({'foo': '1\n2'}, 'foo', ['1', '2']),
        ({'foo': '1\n2\n'}, 'foo', ['1', '2']),
        ({'foo': '1 2\n'}, 'foo', ['1 2']),
    ),
    ids=lambda v: repr(v),
)
def test_convert_to_list(options, option, exp_result, mocker):
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            _config._convert_to_list(options, option)
    else:
        return_value = _config._convert_to_list(options, option)
        assert return_value == exp_result


@pytest.mark.parametrize(
    argnames='options, option, exp_result',
    argvalues=(
        ({'foo': None}, 'foo', None),
        ({'foo': 0}, 'foo', 0),
        ({'foo': ''}, 'foo', 0),
        ({'foo': '0'}, 'foo', 0),
        ({'foo': '02'}, 'foo', 2),
        ({'foo': '002'}, 'foo', 2),
        ({'foo': '0002'}, 'foo', 2),
        ({'foo': '00002'}, 'foo', ValueError('00002: Invalid umask')),
        ({'foo': '777'}, 'foo', 511),
        ({'foo': '0777'}, 'foo', 511),
    ),
    ids=lambda v: repr(v),
)
def test_convert_to_umask(options, option, exp_result, mocker):
    if isinstance(exp_result, Exception):
        with pytest.raises(type(exp_result), match=rf'^{re.escape(str(exp_result))}$'):
            _config._convert_to_umask(options, option)
    else:
        return_value = _config._convert_to_umask(options, option)
        assert return_value == exp_result


def test_Config_reads_file_in_init(mocker):
    filepath = 'path/to/config.cfg'
    config_text = 'mock config'
    mocker.patch('tofipa._config.Config._read', return_value=config_text)
    config = _config.Config(filepath=filepath)
    assert config.filepath == filepath
    assert config._config == config_text
    assert config._read.call_args_list == [call(filepath)]


@pytest.mark.parametrize(
    argnames='is_default, file_exists, permissions, exp_error',
    argvalues=(

        (True, False, None, None),
        (False, False, None, 'Failed to read: No such file or directory'),
        (True, True, None, None),
        (True, True, 0o000, 'Failed to read: Permission denied'),
    ),
)
def test_Config_read(is_default, file_exists, permissions, exp_error, tmp_path, mocker):
    filepath = tmp_path / 'config.cfg'

    mocker.patch('tofipa._config.Config._parse')
    mocker.patch('tofipa._config.Config._ensure_only_valid_options')
    mocker.patch('tofipa._config.Config._fill_in_defaults')
    mocker.patch('tofipa._config.Config._convert_values')

    if is_default:
        mocker.patch('tofipa._config.DEFAULT_CONFIG_FILEPATH', filepath)

    if file_exists:
        config_text = 'foo = bar\n'
        filepath.write_text(config_text)
        if permissions is not None:
            filepath.chmod(permissions)
    else:
        config_text = ''

    if exp_error:
        exp_exception = _errors.ConfigError(exp_error, filepath=filepath)
        with pytest.raises(_errors.ConfigError, match=rf'^{re.escape(str(exp_exception))}$'):
            _config.Config(filepath)
    else:
        config = _config.Config(filepath)
        assert config._config is config._parse.return_value

        assert config._parse.call_args_list == [call(config_text, filepath)]
        assert config._ensure_only_valid_options.call_args_list == [call(config._parse.return_value, filepath)]
        assert config._fill_in_defaults.call_args_list == [call(config._parse.return_value)]
        assert config._convert_values.call_args_list == [call(config._parse.return_value, filepath)]


@pytest.mark.parametrize(
    argnames='text, exp_result, exp_line_number',
    argvalues=(
        ('foo = bar', {'foo': 'bar'}, None),
        ('foo = bar\n', {'foo': 'bar'}, None),
        ('foo = ', {'foo': ''}, None),
        ('foo=bar\n', {'foo': 'bar'}, None),
        ('foo=', {'foo': ''}, None),
        ('foo=\n', {'foo': ''}, None),
        ('foo =\nbar= b a z ', {'foo': '', 'bar': 'b a z'}, None),
        ('foo = 1\n 2 \n  3\n\n', {'foo': '1\n2\n3'}, None),
        ('foo = 1\n 2 \n  3\n\nbar = 4\n', {'foo': '1\n2\n3', 'bar': '4'}, None),
        ('a=0\nfoo = \n  1\n\n    2 \n  3\n\nbar = 4\n', {'a': '0', 'foo': '1\n2\n3', 'bar': '4'}, None),
        ('foo', Exception('foo: Invalid syntax'), 1),
        ('x = 1\nfoo', Exception('foo: Invalid syntax'), 2),
        ('x = 1\ny=2\n\n \n# this is a comment\nfoo\nbar=baz\n', Exception('foo: Invalid syntax'), 6),
    ),
)
def test_Config_parse(text, exp_result, exp_line_number, tmp_path, mocker):
    filepath = tmp_path / 'config.cfg'
    filepath.write_text('')
    config = _config.Config(filepath)

    if isinstance(exp_result, Exception):
        exp_exception = _errors.ConfigError(str(exp_result), filepath=filepath, line_number=exp_line_number)
        with pytest.raises(_errors.ConfigError, match=rf'^{re.escape(str(exp_exception))}$'):
            config._parse(text, filepath)
    else:
        return_value = config._parse(text, filepath)
        assert return_value == exp_result


@pytest.mark.parametrize(
    argnames='valid_values, dct, exp_error',
    argvalues=(
        (('foo', 'bar', 'baz'), {'foo': 'bar'}, None),
        (('foo', 'bar', 'baz'), {'bar': 'baz'}, None),
        (('foo', 'bar', 'baz'), {'baz': 'foo'}, None),
        (('foo', 'bar', 'baz'), {'Baz': 'foo'}, 'Unknown option: Baz'),
        (('foo', 'bar', 'baz'), {'asdf': 'foo'}, 'Unknown option: asdf'),
    ),
    ids=lambda v: repr(v),
)
def test_Config_ensure_only_valid_options__finds_invalid_option(valid_values, dct, exp_error, mocker):
    filepath = 'config.cfg'
    mocker.patch('tofipa._config.Config._read')
    mocker.patch('tofipa._config.Config._valid_options', ('foo', 'bar', 'baz'))
    config = _config.Config(filepath)

    if exp_error:
        exp_exception = _errors.ConfigError(str(exp_error), filepath=filepath)
        with pytest.raises(_errors.ConfigError, match=rf'^{re.escape(str(exp_exception))}$'):
            config._ensure_only_valid_options(dct, filepath)
    else:
        assert config._ensure_only_valid_options(dct, filepath) is None


@pytest.mark.parametrize(
    argnames='defaults, dct, exp_dct',
    argvalues=(
        (
            {'foo': 'default foo', 'bar': 'default bar'},
            {},
            {'foo': 'default foo', 'bar': 'default bar'},
        ),
        (
            {'foo': 'default foo', 'bar': 'default bar'},
            {'foo': 'my special foo'},
            {'foo': 'my special foo', 'bar': 'default bar'},
        ),
        (
            {'foo': 'default foo', 'bar': 'default bar'},
            {'bar': 'my special bar'},
            {'foo': 'default foo', 'bar': 'my special bar'},
        ),
        (
            {'foo': 'default foo', 'bar': 'default bar'},
            {'foo': 'my special foo', 'bar': 'my special bar'},
            {'foo': 'my special foo', 'bar': 'my special bar'},
        ),
        (
            {'foo': 'default foo', 'bar': 'default bar'},
            {'unknown': 'option'},
            {'foo': 'default foo', 'bar': 'default bar', 'unknown': 'option'},
        ),
    ),
    ids=lambda v: repr(v),
)
def test_Config_fill_in_defaults(defaults, dct, exp_dct, mocker):
    mocker.patch('tofipa._config.Config._read')
    config = _config.Config('config.cfg')

    mocker.patch('tofipa._config.Config._defaults', defaults)
    assert config._fill_in_defaults(dct) is None
    assert dct == exp_dct


@pytest.mark.parametrize(
    argnames='dct, convert_values_map, exp_result',
    argvalues=(
        (
            {'foo': '1', 'bar': '2', 'baz': 'BAZ'},
            {'foo': Mock(side_effect=lambda dct, opt: int(dct[opt])),
             'bar': Mock(side_effect=lambda dct, opt: float(dct[opt]))},
            {'foo': 1, 'bar': 2.0, 'baz': 'BAZ'},
        ),
        (
            {'foo': '1', 'bar': '2', 'baz': 'BAZ'},
            {'foo': Mock(side_effect=lambda dct, opt: int(dct[opt])),
             'bar': Mock(side_effect=ValueError('bad value'))},
            Exception('bar: bad value'),
        ),
    ),
    ids=lambda v: repr(v),
)
def test_Config_convert_values(dct, convert_values_map, exp_result, mocker):
    filepath = 'config.cfg'
    mocker.patch('tofipa._config.Config._read')
    config = _config.Config(filepath)
    mocker.patch.object(config, '_convert_values_map', convert_values_map)

    if isinstance(exp_result, Exception):
        exp_exception = _errors.ConfigError(str(exp_result), filepath=filepath)
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            config._convert_values(dct, filepath)

    else:
        assert config._convert_values(dct, filepath) is None
        assert dct == exp_result

    exp_converter_calls = {
        option: [call(dct, option)]
        for option in config._convert_values_map
    }
    for option, converter in config._convert_values_map.items():
        assert converter.call_args_list == exp_converter_calls[option]


def test_Config_filepath(mocker):
    mocker.patch('tofipa._config.Config._read')
    config = _config.Config('config.cfg')

    # Getter
    config._filepath = Mock()
    assert config.filepath is config._filepath

    # Setter
    with pytest.raises(AttributeError):
        config.filepath = 'foo'


def test_Config_getitem(mocker):
    mocker.patch('tofipa._config.Config._read')
    config = _config.Config('config.cfg')
    config._config = {'foo': 1, 'bar': 2, 'baz': 3}
    assert config['foo'] == 1
    assert config['bar'] == 2
    assert config['baz'] == 3


def test_Config_len(mocker):
    mocker.patch('tofipa._config.Config._read')
    config = _config.Config('config.cfg')
    config._config = {'foo': 1, 'bar': 2, 'baz': 3}
    assert len(config) == 3


def test_Config_iter(mocker):
    mocker.patch('tofipa._config.Config._read')
    config = _config.Config('config.cfg')
    config._config = {'foo': 1, 'bar': 2, 'baz': 3}
    assert [x for x in iter(config)] == ['foo', 'bar', 'baz']


def test_Config_repr(mocker):
    mocker.patch('tofipa._config.Config._read')
    config = _config.Config('path/to/config.cfg')
    config._config = {'foo': 1, 'bar': 2, 'baz': 3}
    assert repr(config) == "<Config 'path/to/config.cfg' {'foo': 1, 'bar': 2, 'baz': 3}>"


def test_Locations_locations(mocker):
    mocker.patch('tofipa._config.Locations._read', return_value=['baz'])

    locations = _config.Locations('foo', 'bar', filepath='path/to/locations')

    assert locations == ['foo', 'bar', 'baz']


def test_Locations_filepath(mocker):
    mocker.patch('tofipa._config.Locations._read')

    locations = _config.Locations(filepath='path/to/locations')

    assert locations.filepath == 'path/to/locations'


def test_Locations_repr(mocker):
    mocker.patch('tofipa._config.Locations._read', return_value=['/the/usual/path'])

    locations = _config.Locations(filepath='path/to/locations')
    locations.extend(('/a/path', 'also/this/path'))

    assert repr(locations) == (
        "<Locations"
        " 'path/to/locations'"
        " ['/the/usual/path', '/a/path', 'also/this/path']"
        ">"
    )


def test_Locations_read_reads_filepath(tmp_path, mocker):
    normalize_mock = mocker.patch(
        'tofipa._config.Locations._normalize',
        side_effect=lambda line, fp, ln: (f'normalized {line}',),
    )

    filepath = tmp_path / 'locations'
    filepath.write_text('''
    # A comment
    /path/one
    /path/two

    # Duplicate
    /path/one

       # Another comment
      path/t h r e e 
    '''.strip())  # noqa: W291 trailing whitespace
    locations = _config.Locations('foo', 'bar', filepath=filepath)
    assert locations == [
        'normalized foo',
        'normalized bar',
        'normalized /path/one',
        'normalized /path/two',
        'normalized path/t h r e e',
    ]

    assert normalize_mock.call_args_list == [
        call('foo', None, None),
        call('bar', None, None),
        call('/path/one', filepath, 2),
        call('/path/two', filepath, 3),
        call('/path/one', filepath, 6),
        call('path/t h r e e', filepath, 9),
    ]


def test_Locations_read_handles_nonexisting_default_file(mocker):
    mocker.patch('tofipa._config.Locations._normalize', side_effect=lambda line, fp, ln: ('irrelevant',))
    mocker.patch('tofipa._config.DEFAULT_LOCATIONS_FILEPATH', 'mock/default/locations/file')
    filepath = _config.DEFAULT_LOCATIONS_FILEPATH
    locations = _config.Locations(filepath=filepath)
    assert locations == []


def test_Locations_read_handles_nonexisting_custom_file(mocker):
    mocker.patch('tofipa._config.Locations._normalize', side_effect=lambda line, fp, ln: ('irrelevant',))
    filepath = 'mock/custom/locations/file'
    assert filepath != _config.DEFAULT_LOCATIONS_FILEPATH
    with pytest.raises(_errors.ConfigError, match=rf'^{filepath}: Failed to read: No such file or directory$'):
        _config.Locations(filepath=filepath)


def test_Locations_normalize_expands_subdirectories(mocker, tmp_path):
    parent_directory = tmp_path / 'parent'
    parent_directory.mkdir()
    (parent_directory / 'subdir1').mkdir()
    (parent_directory / 'subdir2').mkdir()
    (parent_directory / 'subdir3').mkdir()
    (parent_directory / 'file1').write_text('foo')
    (parent_directory / 'file2').write_text('bar')

    mocker.patch('tofipa._config.Locations._read')
    filepath = 'mock/locations/file'
    locations = _config.Locations(filepath=filepath)

    return_value = locations._normalize(f'{parent_directory}{os.sep}*', filepath, 123)
    assert return_value == [
        str(parent_directory / 'subdir1'),
        str(parent_directory / 'subdir2'),
        str(parent_directory / 'subdir3'),
    ]


def test_Locations_normalize_handles_exception_from_subdirectories_expansion(mocker, tmp_path):
    parent_directory = tmp_path / 'parent'
    parent_directory.mkdir()
    (parent_directory / 'subdir1').mkdir()
    (parent_directory / 'subdir2').mkdir()
    (parent_directory / 'subdir3').mkdir()
    (parent_directory / 'file1').write_text('foo')
    (parent_directory / 'file2').write_text('bar')
    parent_directory.chmod(0o000)

    mocker.patch('tofipa._config.Locations._read')
    filepath = 'mock/locations/file'
    locations = _config.Locations(filepath=filepath)

    try:
        with pytest.raises(_errors.ConfigError, match=(rf'{filepath}@123: Failed to read subdirectories '
                                                       rf'from {parent_directory}: Permission denied')):
            locations._normalize(f'{parent_directory}{os.sep}*', filepath, 123)
    finally:
        parent_directory.chmod(0o700)


@pytest.mark.parametrize('with_subdir_expansion', (True, False), ids=('with subdir expansion', 'without subdir expansion'))
def test_Locations_normalize_resolves_environment_variables(with_subdir_expansion, mocker):
    mocker.patch('tofipa._config.Locations._read')
    mocker.patch('tofipa._config.Locations._resolve_env_vars',
                 side_effect=lambda string, fp, ln: f'resolved {fp}@{ln}: {string}')

    filepath = 'mock/locations/file'
    locations = _config.Locations(filepath=filepath)

    if with_subdir_expansion:
        mocker.patch('os.listdir', return_value=('a', 'b', 'c'))
        return_value = locations._normalize(f'mock line{os.sep}*', filepath, 123)
        assert return_value == [
            f'resolved {filepath}@123: mock line{os.sep}a',
            f'resolved {filepath}@123: mock line{os.sep}b',
            f'resolved {filepath}@123: mock line{os.sep}c',
        ]
    else:
        return_value = locations._normalize('mock line', filepath, 123)
        assert return_value == [
            f'resolved {filepath}@123: mock line',
        ]


@pytest.mark.parametrize('with_subdir_expansion', (True, False), ids=('with subdir expansion', 'without subdir expansion'))
def test_Locations_normalize_handles_subdir_being_file(with_subdir_expansion, mocker, tmp_path):
    mocker.patch('tofipa._config.Locations._read')

    filepath = 'mock/locations/file'
    locations = _config.Locations(filepath=filepath)

    if with_subdir_expansion:
        parent = tmp_path / 'parent'
        parent.mkdir()
        (parent / 'a').write_text('i am a file')
        (parent / 'b').write_text('i am b file')
        (parent / 'c').write_text('i am c file')
        return_value = locations._normalize(f'{parent}{os.sep}*', filepath, 123)
        assert return_value == []
    else:
        line = tmp_path / 'a file'
        line.write_text('i am a file')
        with pytest.raises(_errors.ConfigError, match=rf'^{filepath}@123: Not a directory: {line}$'):
            locations._normalize(str(line), filepath, 123)


def test_Locations_resolve_env_vars_resolves_tilde(mocker):
    mocker.patch('tofipa._config.Locations._read')
    mocker.patch('os.path.expanduser', return_value='path/with/expanded/tilde')

    filepath = 'mock/locations/file'
    locations = _config.Locations(filepath=filepath)

    return_value = locations._resolve_env_vars('path/with/tilde', filepath, 123)
    assert return_value == 'path/with/expanded/tilde'


def test_Locations_resolve_env_vars_resolves_environment_variables(mocker):
    mocker.patch('tofipa._config.Locations._read')
    mocker.patch.dict('os.environ', {'FOO': 'The Foo', 'bar': 'The Bar'})

    filepath = 'mock/locations/file'
    locations = _config.Locations(filepath=filepath)

    return_value = locations._resolve_env_vars('Foo/$FOO/foo/foo/$bar', filepath, 123)
    assert return_value == 'Foo/The Foo/foo/foo/The Bar'
    return_value = locations._resolve_env_vars('$bar/Bar/BAR/$FOO/bar/$bar', filepath, 123)
    assert return_value == 'The Bar/Bar/BAR/The Foo/bar/The Bar'


def test_Locations_resolve_env_vars_handles_unset_environment_variable(mocker):
    mocker.patch('tofipa._config.Locations._read')
    mocker.patch.dict('os.environ', {'FOO': 'The Foo', 'bar': 'The Bar'})

    filepath = 'mock/locations/file'
    locations = _config.Locations(filepath=filepath)

    with pytest.raises(_errors.ConfigError, match=rf'^{filepath}@123: Unset environment variable: \$baz$'):
        locations._resolve_env_vars('Foo/$FOO/foo/foo/$bar/$baz', filepath, 123)


def test_Locations_resolve_env_vars_handles_empty_environment_variable(mocker):
    mocker.patch('tofipa._config.Locations._read')
    mocker.patch.dict('os.environ', {'FOO': 'The Foo', 'bar': 'The Bar', 'baz': ''})

    filepath = 'mock/locations/file'
    locations = _config.Locations(filepath=filepath)

    with pytest.raises(_errors.ConfigError, match=rf'^{filepath}@123: Empty environment variable: \$baz$'):
        locations._resolve_env_vars('Foo/$FOO/foo/foo/$bar/$baz', filepath, 123)


def test_Locations_normalizes_added_locations(mocker):
    mocker.patch('tofipa._config.Locations._read', return_value=['initial/path'])
    mocker.patch('tofipa._config.Locations._normalize',
                 side_effect=lambda l, f, n: (f'normalized:{l}:{f}:{n}',))

    filepath = 'mock/locations/file'
    locations = _config.Locations(filepath=filepath)
    assert list(locations) == ['initial/path']

    locations.append('appended/path')
    assert list(locations) == ['initial/path', 'normalized:appended/path:None:None']

    locations.insert(1, 'inserted/path')
    assert list(locations) == ['initial/path', 'normalized:inserted/path:None:None', 'normalized:appended/path:None:None']

    del locations[0]
    assert list(locations) == ['normalized:inserted/path:None:None', 'normalized:appended/path:None:None']

    locations.extend(('some', 'more', 'paths'))
    assert list(locations) == ['normalized:inserted/path:None:None', 'normalized:appended/path:None:None',
                               'normalized:some:None:None', 'normalized:more:None:None', 'normalized:paths:None:None']

    locations[1] = 'assigned/path'
    assert list(locations) == ['normalized:inserted/path:None:None', 'normalized:assigned/path:None:None',
                               'normalized:some:None:None', 'normalized:more:None:None', 'normalized:paths:None:None']

    # 'assigned/paths' is ignored because it already exists.
    locations[1] = 'assigned/path'
    assert list(locations) == ['normalized:inserted/path:None:None', 'normalized:assigned/path:None:None',
                               'normalized:some:None:None', 'normalized:more:None:None', 'normalized:paths:None:None']

    # 'paths' is ignored because it already exists.
    locations[1:3] = ('multiple', 'assigned', 'paths')
    assert list(locations) == ['normalized:inserted/path:None:None',
                               'normalized:multiple:None:None', 'normalized:assigned:None:None',
                               'normalized:more:None:None', 'normalized:paths:None:None']


def test_Locations_equality(mocker):
    mocker.patch('tofipa._config.Locations._read', return_value=['initial/path'])

    filepath = 'mock/locations/file'
    locations = _config.Locations(filepath=filepath)

    locations._list = ['a', 'b', 'c']
    assert locations == ['a', 'b', 'c']
    assert locations != ['a', 'c', 'b']

    locations._list = ['a', 'b', 'c']
    assert locations == ('a', 'b', 'c')
    assert locations != ('a', 'b', 'C')

    assert locations != 123


def test_Clients_reads_file_in_init(mocker):
    filepath = 'path/to/clients.ini'
    config_text = 'mock config'
    mocker.patch('tofipa._config.Clients._read', return_value=config_text)
    clients_config = _config.Clients(filepath=filepath)
    assert clients_config.filepath == filepath
    assert clients_config._config == config_text
    assert clients_config._read.call_args_list == [call(filepath)]


@pytest.mark.parametrize(
    argnames='is_default, file_exists, permissions, exp_error',
    argvalues=(
        (True, False, None, None),
        (False, False, None, 'Failed to read: No such file or directory'),
        (True, True, None, None),
        (True, True, 0o000, 'Failed to read: Permission denied'),
    ),
)
def test_Clients_read_fails_to_read(is_default, file_exists, permissions, exp_error, tmp_path, mocker):
    filepath = tmp_path / 'clients.ini'

    if is_default:
        mocker.patch('tofipa._config.DEFAULT_CLIENTS_FILEPATH', filepath)

    if file_exists:
        filepath.write_text('[foo]\nclient = deluge\n\n[bar]\nclient = rtorrent\nverify=true\n')

        if permissions is not None:
            filepath.chmod(permissions)

    if exp_error:
        exp_exception = _errors.ConfigError(exp_error, filepath=filepath)
        with pytest.raises(_errors.ConfigError, match=rf'^{re.escape(str(exp_exception))}$'):
            _config.Clients(filepath)

    else:
        clients_config = _config.Clients(filepath)
        assert isinstance(clients_config._config, dict)


@pytest.mark.parametrize(
    argnames='config_text, exp_error',
    argvalues=(
        ('foo = bar\n', 'Line 1: foo = bar: Option outside of section'),
        ('[foo]\nbar ! baz\n', "Line 2: 'bar ! baz\\n': Invalid syntax"),
        ('[foo]\na = b\n[bar]\nthis = that\n[foo]\n', "Line 5: foo: Duplicate section"),
        ('[foo]\nfoo = 1\nbar = 2\nfoo = 3', "Line 4: foo: Duplicate option"),
    ),
)
def test_Clients_read_fails_to_parse(config_text, exp_error, tmp_path, mocker):
    filepath = tmp_path / 'clients.ini'
    filepath.write_text(config_text)
    exp_exception = _errors.ConfigError(exp_error, filepath=filepath)
    with pytest.raises(_errors.ConfigError, match=rf'^{re.escape(str(exp_exception))}$'):
        _config.Clients(filepath)


def test_Clients_read_catches_generic_error(tmp_path, mocker):
    filepath = tmp_path / 'clients.ini'
    filepath.write_text('[foo]\nx = 1\n')
    mocker.patch('configparser.ConfigParser.read_string', side_effect=configparser.Error('foo!'))
    exp_exception = _errors.ConfigError('foo!', filepath=filepath)
    with pytest.raises(_errors.ConfigError, match=rf'^{re.escape(str(exp_exception))}$'):
        _config.Clients(filepath)


def test_Clients_read_makes_expected_calls(tmp_path, mocker):
    ConfigParser_mock = mocker.patch('configparser.ConfigParser')
    as_dict_mock = mocker.patch('tofipa._config._as_dict')
    mocker.patch('tofipa._config.Clients._ensure_only_valid_options')
    mocker.patch('tofipa._config.Clients._ensure_mandatory_options')
    mocker.patch('tofipa._config.Clients._fill_in_defaults')
    mocker.patch('tofipa._config.Clients._convert_values')

    filepath = tmp_path / 'clients.ini'
    filepath.write_text('[foo]\nx = 1\n')

    clients_config = _config.Clients(filepath)

    assert ConfigParser_mock.call_args_list == [call(
        default_section=None,
        interpolation=None,
        allow_no_value=False,
        delimiters=('=',),
        comment_prefixes=('#',),
    )]
    assert as_dict_mock.call_args_list == [call(ConfigParser_mock.return_value)]
    assert clients_config._ensure_only_valid_options.call_args_list == [call(as_dict_mock.return_value, filepath)]
    assert clients_config._ensure_mandatory_options.call_args_list == [call(as_dict_mock.return_value, filepath)]
    assert clients_config._fill_in_defaults.call_args_list == [call(as_dict_mock.return_value)]
    assert clients_config._convert_values.call_args_list == [call(as_dict_mock.return_value, filepath)]
    assert clients_config._config is as_dict_mock.return_value


def test_Clients_ensure_only_valid_options__finds_invalid_option(mocker):
    mocker.patch('tofipa._config.Clients._read')
    mocker.patch('tofipa._config.Clients._valid_options', ('foo', 'bar', 'baz'))
    clients_config = _config.Clients('clients.ini')

    with pytest.raises(_errors.ConfigError, match='clients.ini: c: Unknown option: asdf'):
        clients_config._ensure_only_valid_options(
            {
                'a': {'foo': 1, 'bar': 2},
                'b': {'bar': 2, 'baz': 3},
                'c': {'foo': 10, 'bar': 20, 'asdf': 1e9, 'baz': 30},
            },
            'clients.ini',
        )

def test_Clients_ensure_only_valid_options__does_not_find_invalid_option(mocker):
    mocker.patch('tofipa._config.Clients._read')
    mocker.patch('tofipa._config.Clients._valid_options', ('foo', 'bar', 'baz'))
    clients_config = _config.Clients('clients.ini')

    clients_config._ensure_only_valid_options(
        {
            'a': {'foo': 1, 'bar': 2},
            'b': {'bar': 2, 'baz': 3},
            'c': {'foo': 10, 'bar': 20, 'baz': 30},
        },
        'clients.ini',
    )


def test_Clients_ensure_mandatory_options__finds_missing_option(mocker):
    mocker.patch('tofipa._config.Clients._read')
    mocker.patch('tofipa._config.Clients._mandatory_options', ('foo', 'bar'))
    clients_config = _config.Clients('clients.ini')

    with pytest.raises(_errors.ConfigError, match='clients.ini: c: Missing option: bar'):
        clients_config._ensure_mandatory_options(
            {
                'a': {'foo': 1, 'bar': 2},
                'b': {'bar': 2, 'foo': 3},
                'c': {'foo': 10, 'baz': 20},
            },
            'clients.ini',
        )


def test_Clients_ensure_mandatory_options__does_not_find_missing_option(mocker):
    mocker.patch('tofipa._config.Clients._read')
    mocker.patch('tofipa._config.Clients._mandatory_options', ('foo', 'bar'))
    clients_config = _config.Clients('clients.ini')

    clients_config._ensure_mandatory_options(
        {
            'a': {'foo': 1, 'bar': 2},
            'b': {'bar': 2, 'foo': 3},
            'c': {'foo': 10, 'bar': 20},
        },
        'clients.ini',
    )


def test_Clients_fill_in_defaults(mocker):
    mocker.patch('tofipa._config.Clients._read')
    clients_config = _config.Clients('clients.ini')

    mocker.patch('tofipa._config.Clients._defaults', {
        'url': 'common default url',
        'username': 'common default username',
    })
    mocker.patch('tofipa._config.Clients._client_defaults', collections.defaultdict(
        lambda: collections.defaultdict(lambda: None),
        {
            'deluge': collections.defaultdict(
                lambda: None,
                {'url': 'default deluge url'},
            ),
            'rtorrent': collections.defaultdict(
                lambda: None,
                {'url': 'default rtorrent url', 'username': 'default rtorrent username'},
            ),
        },
    ))
    dct = {
        'a': {'client': 'deluge', 'username': 'hunter1'},
        'b': {'client': 'transmission', 'url': 'localhost:123', 'password': 'hunter3'},
        'c': {'client': 'rtorrent', 'url': 'localhost:456', 'password': 'hunter4'},
    }
    assert clients_config._fill_in_defaults(dct) is None
    assert dct == {
        'a': {'client': 'deluge', 'username': 'hunter1', 'url': 'default deluge url'},
        'b': {'client': 'transmission', 'url': 'localhost:123', 'password': 'hunter3', 'username': 'common default username'},
        'c': {'client': 'rtorrent', 'url': 'localhost:456', 'password': 'hunter4', 'username': 'default rtorrent username'},
    }


@pytest.mark.parametrize('bad_section', ('a', 'b', 'c', None))
@pytest.mark.parametrize('bad_option', ('client', 'url', 'verify', 'stopped', None))
def test_Clients_convert_values(bad_section, bad_option, mocker):
    filepath = 'clients.ini'
    dct = {
        'a': {'client': 'deluge', 'url': 'http://x:123',
              'username': 'hunter1', 'password': 'hunter2',
              'verify': 'true', 'stopped': 'yes'},
        'b': {'client': 'qbittorrent', 'url': 'http://x:234',
              'username': 'hunter3', 'password': 'hunter4',
              'verify': 'false', 'stopped': 'no'},
        'c': {'client': 'transmission', 'url': 'http://x:345',
              'username': 'hunter4', 'password': 'hunter5',
              'verify': None, 'stopped': None},
    }
    bad_section_index = list(dct).index(bad_section) if bad_section else None

    def convert(options, option):
        section_index = list(dct.values()).index(options)
        if section_index == bad_section_index and option == bad_option:
            raise ValueError(f'Bad value: {options[option]}')
        else:
            return f'{options[option]} proper'

    mocker.patch('tofipa._config._convert_to_client', side_effect=convert)
    mocker.patch('tofipa._config._convert_to_url', side_effect=convert)
    mocker.patch('tofipa._config._convert_to_bool', side_effect=convert)
    mocker.patch('tofipa._config._convert_to_string', side_effect=convert)

    mocker.patch('tofipa._config.Clients._read')
    clients_config = _config.Clients(filepath)

    if bad_section is None or bad_option is None:
        assert clients_config._convert_values(dct, filepath) is None
        assert _config._convert_to_client.call_args_list == [call(dct[id], 'client') for id in dct]
        assert _config._convert_to_url.call_args_list == [call(dct[id], 'url') for id in dct]
        assert _config._convert_to_bool.call_args_list == list(itertools.chain.from_iterable(
            [(call(dct[id], 'verify'), call(dct[id], 'stopped')) for id in dct],
        ))
        assert _config._convert_to_string.call_args_list == list(itertools.chain.from_iterable(
            [(call(dct[id], 'username'), call(dct[id], 'password')) for id in dct],
        ))

        assert dct == {
            'a': {'client': 'deluge proper', 'url': 'http://x:123 proper',
                  'username': 'hunter1 proper', 'password': 'hunter2 proper',
                  'verify': 'true proper', 'stopped': 'yes proper'},
            'b': {'client': 'qbittorrent proper', 'url': 'http://x:234 proper',
                  'username': 'hunter3 proper', 'password': 'hunter4 proper',
                  'verify': 'false proper', 'stopped': 'no proper'},
            'c': {'client': 'transmission proper', 'url': 'http://x:345 proper',
                  'username': 'hunter4 proper', 'password': 'hunter5 proper',
                  'verify': 'None proper', 'stopped': 'None proper'},
        }

    else:
        exp_exception = _errors.ConfigError(
            f'{bad_section}: {bad_option}: Bad value: {dct[bad_section][bad_option]}',
            filepath=clients_config.filepath,
        )
        with pytest.raises(type(exp_exception), match=rf'^{re.escape(str(exp_exception))}$'):
            clients_config._convert_values(dct, filepath)


def test_Clients_filepath(mocker):
    mocker.patch('tofipa._config.Clients._read')
    clients_config = _config.Clients('clients.ini')

    # Getter
    clients_config._filepath = Mock()
    assert clients_config.filepath is clients_config._filepath

    # Setter
    with pytest.raises(AttributeError):
        clients_config.filepath = 'foo'


def test_Clients_default(mocker):
    mocker.patch('tofipa._config.Clients._read', return_value={
        'baz': 'baz config',
        'foo': 'foo config',
        'bar': 'bar config',
    })
    clients_config = _config.Clients('clients.ini')
    assert clients_config.default == 'baz config'


def test_Clients_getitem(mocker):
    mocker.patch('tofipa._config.Clients._read')
    clients_config = _config.Clients('clients.ini')
    clients_config._config = {'foo': 1, 'bar': 2, 'baz': 3}
    assert clients_config['foo'] == 1
    assert clients_config['bar'] == 2
    assert clients_config['baz'] == 3


def test_Clients_len(mocker):
    mocker.patch('tofipa._config.Clients._read')
    clients_config = _config.Clients('clients.ini')
    clients_config._config = {'foo': 1, 'bar': 2, 'baz': 3}
    assert len(clients_config) == 3


def test_Clients_iter(mocker):
    mocker.patch('tofipa._config.Clients._read')
    clients_config = _config.Clients('clients.ini')
    clients_config._config = {'foo': 1, 'bar': 2, 'baz': 3}
    assert [x for x in iter(clients_config)] == ['foo', 'bar', 'baz']


def test_Clients_repr(mocker):
    mocker.patch('tofipa._config.Clients._read')
    clients_config = _config.Clients('path/to/clients.ini')
    clients_config._config = {'foo': 1, 'bar': 2, 'baz': 3}
    assert repr(clients_config) == "<Clients 'path/to/clients.ini' {'foo': 1, 'bar': 2, 'baz': 3}>"
