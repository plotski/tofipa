import pytest

from tofipa import _errors


@pytest.mark.parametrize(
    argnames='args, kwargs, exp_msg',
    argvalues=(
        (('foo',), {}, 'foo'),
        (('foo',), {'filepath': 'path/to/file'}, 'path/to/file: foo'),
        (('foo',), {'filepath': 'path/to/file', 'line_number': 123}, 'path/to/file@123: foo'),
        (('foo',), {'line_number': 123}, 'foo'),
    )
)
def test_ConfigError(args, kwargs, exp_msg):
    e = _errors.ConfigError(*args, **kwargs)
    assert str(e) == exp_msg
