import datetime
import errno
import logging
import os
import shutil
import subprocess
import sys
import types
from unittest.mock import AsyncMock, Mock, call

import pytest
import torf

from tofipa import __project_name__, _cli, _errors


@pytest.mark.skipif(sys.version_info <= (3, 10), reason='requires Python >= 3.10')
def test_parse_args(mocker, capsys):
    mocker.patch('shutil.get_terminal_size', return_value=os.terminal_size((120, 123)))
    mocker.patch('argparse.ArgumentParser.exit')
    argv = ['--help']
    _cli._parse_args(argv)
    out = capsys.readouterr()
    assert out.out == (
        "usage: tofipa [-h] [--config-file CONFIG_FILE] [--location LOCATION [LOCATION ...]] [--locations-file LOCATIONS_FILE]\n"
        "              [--default DEFAULT] [--clients-file CLIENTS_FILE] [--client CLIENT] [--noclient] [--start | --stop]\n"
        "              [--version] [--debug [FILE]]\n"
        "              TORRENT [TORRENT ...]\n"
        "\n"
        "Get download directory from torrent file\n"
        "\n"
        "positional arguments:\n"
        "  TORRENT               Path to torrent file\n"
        "\n"
        "options:\n"
        "  -h, --help            show this help message and exit\n"
        "  --config-file CONFIG_FILE\n"
        "                        File containing configuration options\n"
        "  --location LOCATION [LOCATION ...], -l LOCATION [LOCATION ...]\n"
        "                        Potential download location of existing files in TORRENT (may be given multiple times)\n"
        "  --locations-file LOCATIONS_FILE\n"
        "                        File containing newline-separated list of download locations\n"
        "  --default DEFAULT, -d DEFAULT\n"
        "                        Default location if no existing files are found\n"
        "  --clients-file CLIENTS_FILE\n"
        "                        File containing BitTorrent client connections\n"
        "  --client CLIENT, -c CLIENT\n"
        "                        Add TORRENT to CLIENT (CLIENT is a section name in CLIENTS_FILE)\n"
        "  --noclient, -C        Do not add TORRENT to any client, print download location instead\n"
        "  --start, -s           Start torrent after adding it to a BitTorrent client\n"
        "  --stop, -S            Stop torrent after adding it to a BitTorrent client\n"
        "  --version             show program's version number and exit\n"
        "  --debug [FILE]        Write debugging messages to FILE or STDERR if FILE is \"-\"\n"
    )


def test_fatal_error(mocker, capsys):
    exit_mock = mocker.patch('sys.exit')
    assert _cli._fatal_error('foo') is None
    out = capsys.readouterr()
    assert out.err == 'foo\n'
    assert out.out == ''
    assert exit_mock.call_args_list == [call(1)]


@pytest.mark.parametrize(
    argnames='path, exception, exp_fatal_error_calls',
    argvalues=(
        ('path/to/directory', None, []),
        ('path/to/directory', OSError('cannot create directory'),
         [call('Failed to create directory: path/to/directory: cannot create directory')]),
        ('path/to/directory', OSError(errno.EACCES, 'cannot create directory'),
         [call('Failed to create directory: path/to/directory: cannot create directory')]),
    ),
    ids=lambda v: repr(v),
)
def test_mkdir(path, exception, exp_fatal_error_calls, mocker):
    fatal_error_mock = mocker.patch('tofipa._cli._fatal_error')
    makedirs_mock = mocker.patch('os.makedirs', side_effect=exception)
    assert _cli._mkdir(path) is None
    assert makedirs_mock.call_args_list == [call(path, exist_ok=True)]
    assert fatal_error_mock.call_args_list == exp_fatal_error_calls


@pytest.mark.parametrize(
    argnames='args, exp_mkdir_calls, exp_basicConfig_calls',
    argvalues=(
        (Mock(debug=None), [], []),
        (Mock(debug='-'), [], [call(stream=sys.stderr, level=logging.DEBUG)]),
        (Mock(debug='path/to/file'), [call('path/to')], [call(filename='path/to/file', level=logging.DEBUG)]),
        (Mock(debug='file'), [], [call(filename='file', level=logging.DEBUG)]),
    ),
    ids=lambda v: repr(v),
)
def test_setup_debugging(args, exp_mkdir_calls, exp_basicConfig_calls, mocker):
    mkdir_mock = mocker.patch('tofipa._cli._mkdir')
    basicConfig_mock = mocker.patch('logging.basicConfig')
    assert _cli._setup_debugging(args) is None
    assert mkdir_mock.call_args_list == exp_mkdir_calls
    assert basicConfig_mock.call_args_list == exp_basicConfig_calls


@pytest.mark.parametrize(
    argnames='args, Config_exception, exp_error',
    argvalues=(
        (
            Mock(config_file='path/to/config.cfg'),
            None,
            '',
        ),
        (
            Mock(config_file='path/to/config.cfg'),
            _errors.ConfigError('foo'),
            'foo',
        ),
    ),
)
def test_get_config(args, Config_exception, exp_error, mocker):
    Config_mock = mocker.patch('tofipa._config.Config', side_effect=Config_exception)
    fatal_error_mock = mocker.patch('tofipa._cli._fatal_error')

    return_value = _cli._get_config(args)
    assert Config_mock.call_args_list == [call(filepath=args.config_file)]
    if exp_error:
        assert return_value is None
        assert fatal_error_mock.call_args_list == [call(exp_error)]
    else:
        assert return_value is Config_mock.return_value
        assert fatal_error_mock.call_args_list == []


@pytest.mark.parametrize(
    argnames='args, config_locations, exp_fatal_error_calls, exp_locations',
    argvalues=(
        # Error reading configuration file
        (
            Mock(
                locations_file='path/to/locations',
                location=('cli/location1', 'cli/location2'),
            ),
            _errors.ConfigError('bad config file'),
            [call('bad config file')],
            []
        ),
        # Combined locations from CLI + configuration file
        (
            Mock(
                locations_file='path/to/locations',
                location=('cli/location1', 'cli/location2'),
            ),
            ('config/location1', 'config/location2'),
            [],
            ['cli/location1', 'cli/location2', 'config/location1', 'config/location2'],
        ),
        # Deduplicated locations
        (
            Mock(
                locations_file='path/to/locations',
                location=('location1', 'location2'),
            ),
            ('location3', 'location2', 'location1'),
            [],
            ['location1', 'location2', 'location3'],
        ),
        # No locations anywhere
        (
            Mock(
                locations_file='path/to/locations',
                location=[],
            ),
            [],
            [call(f'No locations specified. See: {__project_name__} --help')],
            [],
        ),
    ),
    ids=lambda v: repr(v),
)
def test_get_locations(args, config_locations, exp_fatal_error_calls, exp_locations, mocker):
    fatal_error_mock = mocker.patch('tofipa._cli._fatal_error')
    if isinstance(config_locations, Exception):
        Locations_mock = mocker.patch('tofipa._config.Locations', side_effect=config_locations)
    else:
        Locations_mock = mocker.patch('tofipa._config.Locations', return_value=list(config_locations))

    locations = _cli._get_locations(args)

    assert Locations_mock.call_args_list == [call(filepath=args.locations_file)]

    if isinstance(config_locations, Exception):
        assert fatal_error_mock.call_args_list
    else:
        assert locations == exp_locations

    assert fatal_error_mock.call_args_list == exp_fatal_error_calls


@pytest.mark.parametrize(
    argnames='args, Clients_result, exp_result',
    argvalues=(
        (
            Mock(clients_file='path/to/clients.ini', client=None),
            _errors.ConfigError('no'),
            [call('no')],
        ),
        (
            Mock(clients_file='path/to/clients.ini', client=None),
            Mock(default={'url': 'defaulthost:123'}),
            {'url': 'defaulthost:123'},
        ),
        (
            Mock(clients_file='path/to/clients.ini', client='deluge'),
            {'deluge': {'url': 'delugehost:123'}, 'rtorrent': {'url': 'rtorrenthost:234'}},
            {'url': 'delugehost:123'},
        ),
        (
            Mock(clients_file='path/to/clients.ini', client='rtorrent'),
            {'deluge': {'url': 'delugehost:123'}, 'rtorrent': {'url': 'rtorrenthost:234'}},
            {'url': 'rtorrenthost:234'},
        ),
        (
            Mock(clients_file='path/to/clients.ini', client='asdf'),
            {'deluge': {'url': 'delugehost:123'}, 'rtorrent': {'url': 'rtorrenthost:234'}},
            [call('Unknown client: asdf')],
        ),
    ),
    ids=lambda v: repr(v),
)
def test_get_client_config(args, Clients_result, exp_result, mocker):
    fatal_error_mock = mocker.patch('tofipa._cli._fatal_error')
    update_client_config_mock = mocker.patch('tofipa._cli._update_client_config',
                                             side_effect=lambda args, config: config)
    if isinstance(Clients_result, Exception):
        Clients_mock = mocker.patch('tofipa._config.Clients', side_effect=Clients_result)
    else:
        Clients_mock = mocker.patch('tofipa._config.Clients', return_value=Clients_result)

    client_config = _cli._get_client_config(args)

    assert Clients_mock.call_args_list == [call(filepath=args.clients_file)]

    if isinstance(exp_result, list):
        assert fatal_error_mock.call_args_list == exp_result
        assert client_config is None
        assert update_client_config_mock.call_args_list == []
    else:
        assert client_config == exp_result
        assert update_client_config_mock.call_args_list == [call(args, exp_result)]


@pytest.mark.parametrize(
    argnames='args, config, exp_config',
    argvalues=(
        (Mock(start=None, stop=None), {'stopped': True}, {'stopped': True}),
        (Mock(start=None, stop=None), {'stopped': False}, {'stopped': False}),

        (Mock(start=False, stop=True), {'stopped': True}, {'stopped': True}),
        (Mock(start=False, stop=True), {'stopped': False}, {'stopped': True}),

        (Mock(start=True, stop=False), {'stopped': True}, {'stopped': False}),
        (Mock(start=True, stop=False), {'stopped': False}, {'stopped': False}),
    ),
    ids=lambda v: repr(v),
)
def test_update_client_config(args, config, exp_config):
    return_value = _cli._update_client_config(args, config)
    assert return_value == exp_config


@pytest.mark.parametrize(
    argnames='args, client_config, exp_Client_calls, exp_return_value',
    argvalues=(
        (Mock(noclient=True), {'client': 'config'}, [], None),
        (Mock(noclient=False), {'client': 'config'}, [call({'client': 'config'})], 'client object'),
        (Mock(noclient=False), None, [], None),
    ),
    ids=lambda v: repr(v),
)
def test_get_client(args, client_config, exp_Client_calls, exp_return_value, mocker):
    get_client_config_mock = mocker.patch('tofipa._cli._get_client_config', return_value=client_config)
    Client_mock = mocker.patch('tofipa._btclient.Client')

    config = _cli._get_client(args)
    assert Client_mock.call_args_list == exp_Client_calls

    if exp_return_value is None:
        assert config is None
    else:
        assert config is Client_mock.return_value

    if not args.noclient:
        assert get_client_config_mock.call_args_list == [call(args)]


@pytest.mark.parametrize('umask', (None, 0, 0o001))
@pytest.mark.parametrize('umask_orig', (0, 0o002))
@pytest.mark.parametrize(
    argnames='exception_raised_by, exp_return_value',
    argvalues=(
        (None, 'return value from find()'),
        ('find', None),
        ('create_links', None),
    ),
    ids=lambda v: repr(v),
)
def test_find_location(exception_raised_by, umask, umask_orig, exp_return_value, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('os.umask', return_value=umask_orig), 'umask')
    mocks.attach_mock(mocker.patch('tofipa._cli._fatal_error'), 'fatal_error')
    mocks.attach_mock(mocker.patch('tofipa._cli._get_locations'), 'get_locations')
    mocks.attach_mock(Mock(return_value='return value from find()'), 'find')
    mocks.attach_mock(Mock(), 'create_links')
    mocks.attach_mock(
        mocker.patch('tofipa._location.FindDownloadLocation', return_value=Mock(
            find=mocks.find,
            create_links=mocks.create_links,
        )),
        'FindDownloadLocation',
    )

    if exception_raised_by:
        mock = getattr(mocks, exception_raised_by)
        mock.side_effect = _errors.FindError('oh no')

    args = Mock()
    torrent = Mock()
    config = {'umask': umask}

    return_value = _cli._find_location(torrent, args, config)
    if exp_return_value is None:
        assert return_value is None
    else:
        assert return_value is mocks.find.return_value

    exp_mock_calls = [
        call.get_locations(args),
        call.FindDownloadLocation(
            torrent=torrent,
            locations=mocks.get_locations.return_value,
            default=args.default,
        ),
        call.find(),
    ]
    if exception_raised_by != 'find':
        if umask is not None:
            exp_mock_calls.append(call.umask(umask))

        exp_mock_calls.append(call.create_links(mocks.find.return_value))

        if umask is not None:
            exp_mock_calls.append(call.umask(umask_orig))

    if exception_raised_by:
        exp_mock_calls.append(call.fatal_error('oh no'))

    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='location, torrent, client, exp_stderr, exp_stdout, exp_return_value, exp_client_calls',
    argvalues=(
        ('/path/to/location', 'path/to.torrent', None, '', '/path/to/location\n', True, []),
        (
            '/path/to/location',
            'path/to.torrent',
            Mock(add_torrent=AsyncMock(return_value=Mock(
                warnings=[],
                errors=[],
            ))),
            '',
            '',
            True,
            [call.add_torrent('path/to.torrent', '/path/to/location')],
        ),
        (
            '/path/to/location',
            'path/to.torrent',
            Mock(add_torrent=AsyncMock(return_value=Mock(
                warnings=['Heads up!', 'Look out!'],
                errors=[],
            ))),
            'path/to.torrent: WARNING: Heads up!\npath/to.torrent: WARNING: Look out!\n',
            '',
            True,
            [call.add_torrent('path/to.torrent', '/path/to/location')],
        ),
        (
            '/path/to/location',
            'path/to.torrent',
            Mock(add_torrent=AsyncMock(return_value=Mock(
                warnings=[],
                errors=['This is bad.', 'Real bad.'],
            ))),
            'path/to.torrent: This is bad.\npath/to.torrent: Real bad.\n',
            '',
            False,
            [call.add_torrent('path/to.torrent', '/path/to/location')],
        ),
        (
            '/path/to/location',
            'path/to.torrent',
            Mock(add_torrent=AsyncMock(return_value=Mock(
                warnings=['Heads up!', 'Look out!'],
                errors=['This is bad.', 'Real bad.'],
            ))),
            (
                'path/to.torrent: WARNING: Heads up!\npath/to.torrent: WARNING: Look out!\n'
                'path/to.torrent: This is bad.\npath/to.torrent: Real bad.\n'
            ),
            '',
            False,
            [call.add_torrent('path/to.torrent', '/path/to/location')],
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test_handle_location(location, torrent, client, exp_stderr, exp_stdout, exp_return_value, exp_client_calls, capsys):
    return_value = await _cli._handle_location(location, torrent, client)
    assert return_value is exp_return_value
    out = capsys.readouterr()
    assert out.err == exp_stderr
    assert out.out == exp_stdout

    if client:
        assert client.mock_calls == exp_client_calls
    else:
        assert client is None


@pytest.mark.parametrize(
    argnames='commands, bad_command, exception, exp_error',
    argvalues=(
        (
            ['foo --hey', 'bar --ho', 'baz'],
            'foo --hey',
            subprocess.CalledProcessError(12, 'ignored'),
            'Command died with exit code 12: foo --hey',
        ),
        (
            ['foo --hey', 'bar --ho', 'baz'],
            'bar --ho',
            subprocess.CalledProcessError(23, 'ignored'),
            'Command died with exit code 23: bar --ho',
        ),
        (
            ['foo --hey', 'bar --ho', 'baz'],
            'baz',
            subprocess.CalledProcessError(123, 'ignored'),
            'Command died with exit code 123: baz',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_run_shell_commands(commands, bad_command, exception, exp_error, mocker):
    def run(cmd, *args, **kwargs):
        if cmd == bad_command:
            raise exception

    run_mock = mocker.patch('subprocess.run', side_effect=run)
    fatal_error_mock = mocker.patch('tofipa._cli._fatal_error')

    env = {'one': 'two'}
    _cli._run_shell_commands(commands, env=env)

    exp_run_calls = [
        call(cmd, shell=True, check=True, env=env)
        for cmd in commands
    ]
    assert run_mock.call_args_list == exp_run_calls

    if exception:
        fatal_error_mock.call_args_list == [call(exp_error)]
    else:
        fatal_error_mock.call_args_list == []


@pytest.mark.parametrize(
    argnames='Torrent_read_exception',
    argvalues=(
        None,
        torf.TorfError('foo'),
    ),
    ids=lambda v: repr(v),
)
def test_get_torrent(Torrent_read_exception, mocker):
    fatal_error_mock = mocker.patch('tofipa._cli._fatal_error')
    Torrent_read_mock = mocker.patch('torf.Torrent.read', side_effect=Torrent_read_exception)

    return_value = _cli._get_torrent('path/to.torrent')
    assert Torrent_read_mock.call_args_list == [call('path/to.torrent')]

    if Torrent_read_exception:
        assert fatal_error_mock.call_args_list == [call(str(Torrent_read_exception))]
        assert return_value is None
    else:
        assert fatal_error_mock.call_args_list == []
        assert return_value is Torrent_read_mock.return_value


@pytest.mark.parametrize(
    argnames='trackers, exp_return_value',
    argvalues=(
        ([], 'notracker'),
        ([Mock(hostname='127.0.0.1'), Mock(hostname='tracker2.org')], '127.0.0.1'),
        ([Mock(hostname='127.0.0.1:6881'), Mock(hostname='tracker2.org')], '127.0.0.1'),
        ([Mock(hostname='::1'), Mock(hostname='tracker2.org')], '::1'),
        ([Mock(hostname='[::1]:6881'), Mock(hostname='tracker2.org')], '::1'),

        ([Mock(hostname='localhost'), Mock(hostname='tracker2.org')], 'localhost'),
        ([Mock(hostname='localhost:6881'), Mock(hostname='tracker2.org')], 'localhost'),
        ([Mock(hostname='tracker1.org'), Mock(hostname='tracker2.org')], 'tracker1'),
        ([Mock(hostname='tracker1.org:6881'), Mock(hostname='tracker2.org')], 'tracker1'),
    ),
    ids=lambda v: repr(v),
)
def test_get_tracker_name(trackers, exp_return_value):
    torrent = Mock(trackers=Mock(flat=trackers))
    return_value = _cli._get_tracker_name(torrent)
    assert return_value == exp_return_value


@pytest.mark.parametrize(
    argnames='date, fmt, default, exp_return_value',
    argvalues=(
        (None, '%Y-%m-%d', 'nothing', 'nothing'),
        (datetime.datetime.fromisoformat('2000-01-02T03:04:05.123'), '%Y/%m/%d', 'nothing', '2000/01/02'),
    ),
    ids=lambda v: repr(v),
)
def test_format_datetime(date, fmt, default, exp_return_value):
    return_value = _cli._format_datetime(date, fmt, default)
    assert return_value == exp_return_value


@pytest.mark.parametrize(
    argnames='torrent_attrs, filename, template, exp_result',
    argvalues=(
        # Unknown placeholder
        (
            {
                'creation_date': 'mock datetime',
                'infohash': 'd34db33f',
                'name': 'My Torrent',
                'trackers': 'mock trackers',
            },
            'my.torrent',
            'path/to/{foo}',
            Exception('Unknown placeholder in path/to/{foo}: foo'),
        ),
        # Invalid template
        (
            {
                'creation_date': 'mock datetime',
                'infohash': 'd34db33f',
                'name': 'My Torrent',
                'trackers': 'mock trackers',
            },
            'my.torrent',
            'path/to/{foo',
            Exception('Invalid template: path/to/{foo'),
        ),
        # Current time placeholders
        (
            {
                'creation_date': 'mock datetime',
                'infohash': 'd34db33f',
                'name': 'My Torrent',
                'trackers': 'mock trackers',
            },
            'my.torrent',
            'path/to/{filename}/{infohash}/{name}/{current_date}.{current_datetime}.{current_time}',
            'path/to/my.torrent/d34db33f/My Torrent/{current_date}.{current_datetime}.{current_time}/my.torrent',
        ),
        # Torrent time placeholders
        (
            {
                'creation_date': 'mock datetime',
                'infohash': 'd34db33f',
                'name': 'My Torrent',
                'trackers': 'mock trackers',
            },
            'my.torrent',
            'path/to/{filename}/{infohash}/{name}/{torrent_date}.{torrent_datetime}.{torrent_time}',
            'path/to/my.torrent/d34db33f/My Torrent/nodate.nodatetime.notime/my.torrent',
        ),
        # File path instead of directory file
        (
            {
                'creation_date': 'mock datetime',
                'infohash': 'd34db33f',
                'name': 'My Torrent',
                'trackers': 'mock trackers',
            },
            'my.torrent',
            'path/to/{tracker}/{name}.{infohash}.torrent',
            'path/to/<tracker name>/My Torrent.d34db33f.torrent',
        ),
    ),
    ids=lambda v: repr(v),
)
def test_get_torrent_target_filepath(torrent_attrs, filename, template, exp_result, mocker):
    fatal_error_mock = mocker.patch('tofipa._cli._fatal_error')
    format_datetime_mock = mocker.patch('tofipa._cli._format_datetime', side_effect=lambda t, fmt, default: default)
    get_tracker_name_mock = mocker.patch('tofipa._cli._get_tracker_name', return_value='<tracker name>')
    now_mock = Mock(return_value=Mock(strftime=Mock(
        side_effect=lambda fmt: f'<{fmt}>',
    )))
    mocker.patch('datetime.datetime', Mock(now=now_mock))

    torrent = Mock(spec=tuple(torrent_attrs))
    torrent.configure_mock(**torrent_attrs)

    return_value = _cli._get_torrent_target_filepath(torrent, template, filename)

    if isinstance(exp_result, Exception):
        assert return_value is None
        assert fatal_error_mock.call_args_list == [call(str(exp_result))]
    else:
        exp_result = exp_result.format(
            current_date='<%Y-%m-%d>',
            current_datetime='<%Y-%m-%dT%H:%M:%S>',
            current_time='<%H:%M:%S>',
        )
        assert return_value == exp_result
        assert fatal_error_mock.call_args_list == []

    format_datetime_mock.call_args_list == [
        call(torrent.creation_date, '%Y-%m-%d', 'nodate'),
        call(torrent.creation_date, '%Y-%m-%dT%H:%M:%S', 'nodatetime'),
        call(torrent.creation_date, '%H:%M:%S', 'notime'),
    ]
    get_tracker_name_mock.call_args_list == [call(torrent)]


@pytest.mark.parametrize(
    argnames='source_path, target_path, copy_exception, exp_error',
    argvalues=(
        ('source/path.torrent', None, None,
         None),
        ('source/path.torrent', 'target/path.torrent', None,
         None),
        ('source/path.torrent', 'target/path.torrent', None,
         'Failed to create directory: target: cannot create directory'),
        ('source/path.torrent', 'target/path.torrent', shutil.SameFileError('already copied earlier'),
         None),
        ('source/path.torrent', 'target/path.torrent', OSError('cannot copy'),
         'Failed to copy source/path.torrent: cannot copy'),
        ('source/path.torrent', 'target/path.torrent', OSError(errno.EACCES, 'cannot copy'),
         'Failed to copy source/path.torrent: cannot copy'),
    ),
    ids=lambda v: repr(v),
)
def test_copy_torrent(source_path, target_path, copy_exception, exp_error, mocker):
    mocks = Mock()
    mocks.attach_mock(mocker.patch('tofipa._cli._fatal_error'), 'fatal_error')
    mocks.attach_mock(
        mocker.patch('tofipa._cli._get_torrent_target_filepath', return_value=target_path),
        'get_torrent_target_filepath',
    )
    mocks.attach_mock(mocker.patch('tofipa._cli._mkdir'), 'mkdir')
    mocks.attach_mock(mocker.patch('shutil.copy', side_effect=copy_exception), 'copy')

    config = {'copy_torrent_to': 'target/path'}
    torrent = Mock()

    _cli._copy_torrent(config, source_path, torrent)

    exp_mock_calls = [
        call.get_torrent_target_filepath(
            torrent,
            config['copy_torrent_to'],
            'path.torrent',
        ),
    ]
    if target_path:
        exp_target_dir = os.path.dirname(target_path)
        exp_mock_calls.append(
            call.mkdir(exp_target_dir)
        )
        exp_mock_calls.append(call.copy(source_path, target_path))
        if copy_exception and exp_error:
            exp_mock_calls.append(call.fatal_error(exp_error))

    print('CALLS EXPECTED:')
    for c in exp_mock_calls:
        print(c)

    print('CALLS MADE:')
    for c in mocks.mock_calls:
        print(c)

    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='location, torrent_paths,  handle_location_return_values, exp_exit_code',
    argvalues=(
        (
            'path/to/downloads',
            ['yip.torrent', 'yep.torrent', 'yoop.torrent'],
            [True, True, True],
            0,
        ),
        (
            'path/to/downloads',
            ['yip.torrent', 'yep.torrent', 'yoop.torrent'],
            [True, True, False],
            1,
        ),
        (
            'path/to/downloads',
            ['yip.torrent', 'yep.torrent', 'yoop.torrent'],
            [True, False, True],
            1,
        ),
        (
            'path/to/downloads',
            ['yip.torrent', 'yep.torrent', 'yoop.torrent'],
            [False, False, False],
            1,
        ),
    ),
    ids=lambda v: repr(v),
)
@pytest.mark.asyncio
async def test__cli(location, torrent_paths, handle_location_return_values, exp_exit_code, mocker):
    argv = ['foo', 'bar', 'baz']
    client = Mock()
    config = {
        'before_location_search_commands': 'mock before_location_search_commands',
        'after_location_found_commands': 'mock after_location_found_commands',
        'after_torrent_handled_commands': 'mock after_torrent_handled_commands',
        'umask': 'mock umask',
    }

    mocks = Mock()
    mocks.attach_mock(
        mocker.patch('tofipa._cli._parse_args', return_value=Mock(TORRENT=torrent_paths)),
        'parse_args',
    )
    mocks.attach_mock(
        mocker.patch('tofipa._cli._setup_debugging'),
        'setup_debugging',
    )
    mocks.attach_mock(
        mocker.patch('tofipa._cli._get_config', return_value=config),
        'get_config',
    )
    mocks.attach_mock(
        mocker.patch('tofipa._cli._get_client', return_value=client),
        'get_client',
    )
    mocks.attach_mock(
        mocker.patch('tofipa._cli._get_torrent', side_effect=[
            types.SimpleNamespace(name=torrent_file[:-len('.torrent')]) for torrent_file in torrent_paths
        ]),
        'get_torrent',
    )
    mocks.attach_mock(
        mocker.patch('tofipa._cli._copy_torrent'),
        'copy_torrent',
    )
    mocks.attach_mock(
        mocker.patch('tofipa._cli._find_location', return_value=location),
        'find_location',
    )
    mocks.attach_mock(
        mocker.patch('tofipa._cli._handle_location', side_effect=handle_location_return_values),
        'handle_location',
    )
    mocks.attach_mock(
        mocker.patch('tofipa._cli._run_shell_commands'),
        'run_shell_commands',
    )
    args = mocks.parse_args.return_value

    exit_code = await _cli._cli(argv)
    assert exit_code == exp_exit_code

    exp_mock_calls = [
        call.parse_args(argv),
        call.setup_debugging(args),
        call.get_config(args),
        call.get_client(args),
    ]

    torrents = {}
    for i, torrent_path in enumerate(torrent_paths):
        exp_mock_calls.append(
            call.get_torrent(torrent_path)
        )
        torrents[torrent_path] = types.SimpleNamespace(name=torrent_path[:-len('.torrent')])

    for i, torrent_path in enumerate(torrent_paths):
        exp_mock_calls.append(
            call.copy_torrent(config, torrent_path, torrents[torrent_path])
        )

    for i, torrent_path in enumerate(torrent_paths):
        torrent_name = torrents[torrent_path].name
        exp_mock_calls.extend((
            call.run_shell_commands(config['before_location_search_commands'], env={
                'TOFIPA_TORRENT_FILE': torrent_path,
                'TOFIPA_TORRENT_NAME': torrent_name,
                'TOFIPA_TORRENT_LOCATION': '',
                'TOFIPA_TORRENT_PATH': '',
            }),
            call.find_location(torrent_path, args, config),
            call.run_shell_commands(config['after_location_found_commands'], env={
                'TOFIPA_TORRENT_FILE': torrent_path,
                'TOFIPA_TORRENT_NAME': torrent_name,
                'TOFIPA_TORRENT_LOCATION': mocks.find_location.return_value,
                'TOFIPA_TORRENT_PATH': os.path.join(
                    mocks.find_location.return_value,
                    torrent_path[:-len('.torrent')],
                ),
            }),
        ))

        exp_mock_calls.append(
            call.handle_location(mocks.find_location.return_value, torrent_path, client),
        )

        if handle_location_return_values[i] is True:
            exp_mock_calls.append(
                call.run_shell_commands(config['after_torrent_handled_commands'], env={
                    'TOFIPA_TORRENT_FILE': torrent_path,
                    'TOFIPA_TORRENT_NAME': torrent_name,
                    'TOFIPA_TORRENT_LOCATION': mocks.find_location.return_value,
                    'TOFIPA_TORRENT_PATH': os.path.join(
                        mocks.find_location.return_value,
                        torrent_path[:-len('.torrent')],
                    ),
                }),
            )

    print('CALLS EXPECTED:')
    for c in exp_mock_calls:
        print(c)

    print('CALLS MADE:')
    for c in mocks.mock_calls:
        print(c)

    assert mocks.mock_calls == exp_mock_calls


@pytest.mark.parametrize(
    argnames='exception, exp_error',
    argvalues=(
        (None, None),
        (KeyboardInterrupt, 'Cancelled'),
    ),
)
def test_cli(exception, exp_error, mocker):
    exit_mock = mocker.patch('sys.exit')
    run_mock = mocker.patch('asyncio.run', Mock(side_effect=exception))
    cli_mock = mocker.patch('tofipa._cli._cli', Mock())
    fatal_error_mock = mocker.patch('tofipa._cli._fatal_error')
    mocker.patch('sys.argv', [__project_name__, 'foo', '--bar'])

    _cli.cli()
    assert cli_mock.call_args_list == [call(['foo', '--bar'])]
    assert run_mock.call_args_list == [call(cli_mock.return_value)]
    if exp_error:
        assert exit_mock.call_args_list == []
        assert fatal_error_mock.call_args_list == [call(exp_error)]
    else:
        assert exit_mock.call_args_list == [call(run_mock.return_value)]
        assert fatal_error_mock.call_args_list == []
