from unittest.mock import AsyncMock, Mock, PropertyMock, call

import pytest

from tofipa._btclient import Client


def test_Client_config(mocker):
    initial_config = {
        'client': 'testie',
        'url': 'testie://localhost',
    }
    client = Client(initial_config)
    config = client.config
    assert client.config == config == initial_config == {
        'client': 'testie',
        'url': 'testie://localhost',
    }

    # client.config returns copy of internal config
    for option in client.config:
        client.config[option] = 'foo'
    assert client.config == config == initial_config == {
        'client': 'testie',
        'url': 'testie://localhost',
    }

    for option in config:
        config[option] = 'bar'
    assert client.config == initial_config
    assert client.config != config

    for option in initial_config:
        initial_config[option] = 'bar'
    assert client.config != initial_config
    assert client.config != config
    assert initial_config == config


def test_Client_api(mocker):
    api_mock = mocker.patch('aiobtclientapi.api')
    client = Client({
        'client': 'testie',
        'url': 'testie://localhost',
        'username': 'hunter1',
        'password': 'hunter2',
    })
    for _ in range(3):
        assert client.api is api_mock.return_value
    assert api_mock.call_args_list == [call(
        name='testie',
        url='testie://localhost',
        username='hunter1',
        password='hunter2',
    )]


@pytest.mark.asyncio
async def test_Client_add_torrent(mocker):
    client = Client({'verify': 'why not', 'stopped': 'nah'})
    context_manager = Mock(
        __aenter__=AsyncMock(return_value=client),
        __aexit__=AsyncMock(return_value=False),
        add=AsyncMock(return_value='torrent added'),
    )
    mocker.patch.object(type(client), 'api', PropertyMock(
        return_value=context_manager,
    ))
    return_value = await client.add_torrent('foo.torrent', 'path/to/location')
    assert return_value == 'torrent added'
    assert client.api.add.call_args_list == [call(
        'foo.torrent',
        location='path/to/location',
        verify='why not',
        stopped='nah',
    )]
